package mower;

public record Garden(Dimension limit) {
    public boolean isInBound(Position position) {
        return position.x() <= this.limit.x()
                && position.x() >= 0
                && position.y() <= this.limit.y()
                && position.y() >= 0;
    }
}

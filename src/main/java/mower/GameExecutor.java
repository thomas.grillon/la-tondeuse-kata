package mower;

import java.util.List;

public interface GameExecutor {
    List<MowerState> play();
}

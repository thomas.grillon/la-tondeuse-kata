package mower;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class GardenShould {
    private static Stream<Arguments> providerArgumentsIsInGardenBound() {
        return Stream.of(
                Arguments.of(new Dimension(5, 5), new Position(1, 2)),
                Arguments.of(new Dimension(5, 5), new Position(0, 0)),
                Arguments.of(new Dimension(5, 5), new Position(5, 5)),
                Arguments.of(new Dimension(5, 5), new Position(2, 2)),
                Arguments.of(new Dimension(0, 0), new Position(0, 0))
        );
    }
    private static Stream<Arguments> providerArgumentsIsNotInGardenBound() {
        return Stream.of(
                Arguments.of(new Dimension(5, 5), new Position(6, 2)),
                Arguments.of(new Dimension(5, 5), new Position(-1, 6)),
                Arguments.of(new Dimension(5, 5), new Position(-2, -2)),
                Arguments.of(new Dimension(0, 0), new Position(5, 0))
        );
    }


    @ParameterizedTest
    @MethodSource("providerArgumentsIsInGardenBound")
    void returnTrueIfPositionIsInGardenBound(Dimension dimension, Position position) {
        //Arrange
        final var garden = new Garden(dimension);

        //Act
        final var actualIsInBound = garden.isInBound(position);

        //Assertions
        Assertions.assertTrue(actualIsInBound);
    }

    @ParameterizedTest
    @MethodSource("providerArgumentsIsNotInGardenBound")
    void returnFalseIfPositionIsNotInGardenBound(Dimension dimension, Position position) {
        //Arrange
        final var garden = new Garden(dimension);

        //Act
        final var actualIsInBound = garden.isInBound(position);

        //Assertions
        Assertions.assertFalse(actualIsInBound);
    }
}

package input;

import mower.*;

import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultGameParser implements GameParser {

    @Override
    public ParsedContent parse(List<String> content) throws ParsingException {
        try {
            final var state = new ArrayList<MowerState>();

            final var lineOne = List.of(content.get(0).split(" "));
            final var garden = new Garden(new Dimension(Integer.parseInt(lineOne.get(0)), Integer.parseInt(lineOne.get(1))));

            for (int i = 1; i < content.size(); i += 2) {
                final var stateLine = List.of(content.get(i).split(" "));
                final var actionsLine = List.of(content.get(i + 1).split(""));

                state.add(new MowerState(
                                i,
                                new Position(Integer.parseInt(stateLine.get(0)), Integer.parseInt(stateLine.get(1))),
                                getOrientation(stateLine.get(2)),
                                actionsLine.stream().map(this::getMovement).collect(Collectors.toList())
                        ));
            }

            return new ParsedContent(garden, state);
        } catch (Exception e) {
            throw new ParsingException(String.format("Error while parsing: %s", e.getMessage()));
        }
    }

    private Movement getMovement(String action) {
        return switch (action) {
            case "A" -> Movement.GO_FORWARD;
            case "D" -> Movement.TURN_RIGHT;
            case "G" -> Movement.TURN_LEFT;
            case "M" -> Movement.MAX_FORWARD;
            default -> throw new UnsupportedOperationException(String.format("Unknown action %s", action));
        };
    }

    private Orientation getOrientation(String orientation) {
        return switch(orientation) {
            case "N" -> Orientation.NORTH;
            case "S" -> Orientation.SOUTH;
            case "E" -> Orientation.EAST;
            case "W" -> Orientation.WEST;
            default -> throw new UnsupportedOperationException(String.format("Unknown orientation %s", orientation));
        };
    }
}

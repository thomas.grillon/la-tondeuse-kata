package mower;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

public class TwoDimensionMowerMoverShould {

    private static Stream<Arguments> provideArgumentsGoForward() {
        return Stream.of(
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.NORTH, List.of(Movement.GO_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 2), Orientation.NORTH, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.EAST, List.of(Movement.GO_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(2, 1), Orientation.EAST, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.SOUTH, List.of(Movement.GO_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 0), Orientation.SOUTH, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.WEST, List.of(Movement.GO_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(0, 1), Orientation.WEST, List.of(Movement.GO_FORWARD))
                )
        );
    }

    private static Stream<Arguments> provideArgumentsTurnLeft() {
        return Stream.of(
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.NORTH, List.of(Movement.TURN_LEFT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.WEST, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.EAST, List.of(Movement.TURN_LEFT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.NORTH, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.SOUTH, List.of(Movement.TURN_LEFT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.EAST, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.WEST, List.of(Movement.TURN_LEFT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.SOUTH, List.of(Movement.GO_FORWARD))
                )
        );
    }

    private static Stream<Arguments> provideArgumentsTurnRight() {
        return Stream.of(
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.NORTH, List.of(Movement.TURN_RIGHT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.EAST, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.EAST, List.of(Movement.TURN_RIGHT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.SOUTH, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.SOUTH, List.of(Movement.TURN_RIGHT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.WEST, List.of(Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.WEST, List.of(Movement.TURN_RIGHT, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 1), Orientation.NORTH, List.of(Movement.GO_FORWARD))
                )
        );
    }

    private static Stream<Arguments> provideArgumentsGoMaxForward() {
        return Stream.of(
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.NORTH, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 2), Orientation.NORTH, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.EAST, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(2, 1), Orientation.EAST, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.SOUTH, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(1, 0), Orientation.SOUTH, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD))
                ),
                Arguments.of(
                        new MowerState(1, new Position(1, 1), Orientation.WEST, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD)),
                        new MowerState(1, new Position(0, 1), Orientation.WEST, List.of(Movement.MAX_FORWARD, Movement.GO_FORWARD))
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsGoForward")
    void returnNextMowerStateAfterGoForward(MowerState initialState, MowerState expectedState) {
        //Arrange
        final var mover = new TwoDimensionMowerMover();

        //Act
        final var actualState = mover.move(initialState);

        //Assertions
        Assertions.assertEquals(expectedState, actualState);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsTurnLeft")
    void returnNextMowerStateAfterTurnLeft(MowerState initialState, MowerState expectedState) {
        //Arrange
        final var mover = new TwoDimensionMowerMover();

        //Act
        final var actualState = mover.move(initialState);

        //Assertions
        Assertions.assertEquals(expectedState, actualState);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsTurnRight")
    void returnNextMowerStateAfterTurnRight(MowerState initialState, MowerState expectedState) {
        //Arrange
        final var mover = new TwoDimensionMowerMover();

        //Act
        final var actualState = mover.move(initialState);

        //Assertions
        Assertions.assertEquals(expectedState, actualState);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsGoMaxForward")
    void returnNextMowerStateAfterGoMaxForward(MowerState initialState, MowerState expectedState) {
        //Arrange
        final var mover = new TwoDimensionMowerMover();

        //Act
        final var actualState = mover.move(initialState);

        //Assertions
        Assertions.assertEquals(expectedState, actualState);
    }
}

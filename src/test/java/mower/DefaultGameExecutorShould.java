package mower;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultGameExecutorShould {

    @Mock MowerExecutor mowerExecutor;

    @Test
    void returnTheRightFinalState() {
        //Arrange
        final var actionsListMowerOne = List.of(Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.GO_FORWARD);
        final var actionsListMowerTwo = List.of(Movement.GO_FORWARD, Movement.GO_FORWARD, Movement.TURN_RIGHT, Movement.GO_FORWARD, Movement.GO_FORWARD, Movement.TURN_RIGHT, Movement.GO_FORWARD, Movement.TURN_RIGHT, Movement.TURN_RIGHT, Movement.GO_FORWARD);
        final var mowerOne = new MowerState(1, new Position(1, 2), Orientation.NORTH, actionsListMowerOne);
        final var mowerTwo = new MowerState(2, new Position(3, 3), Orientation.EAST, actionsListMowerTwo);

        final var garden = new Garden(new Dimension(5, 5));
        final var initialState = List.of(mowerOne, mowerTwo);

        final var finalStateOne = new MowerState(1, new Position(1, 3), Orientation.NORTH, List.of());
        final var finalStateTwo =new MowerState(2, new Position(5, 1), Orientation.EAST, List.of());

        final var expectedFinalState = List.of(finalStateOne, finalStateTwo);

        //Stubbing
        when(this.mowerExecutor.moveMower(eq(mowerOne), eq(List.of(mowerTwo)), eq(garden))).thenReturn(List.of(
            finalStateOne, mowerTwo
        ));
        when(this.mowerExecutor.moveMower(eq(mowerTwo), eq(List.of(finalStateOne)), eq(garden))).thenReturn(List.of(
                finalStateOne, finalStateTwo
        ));

        //Act
        final var gameExecutor = new DefaultGameExecutor(garden, initialState, this.mowerExecutor);
        final var actualFinalState = gameExecutor.play();

        //Assert
        InOrder inOrder = Mockito.inOrder(this.mowerExecutor);
        Assertions.assertEquals(expectedFinalState, actualFinalState);
        inOrder.verify(this.mowerExecutor).moveMower(mowerOne, List.of(mowerTwo), garden);
        inOrder.verify(this.mowerExecutor).moveMower(mowerTwo, List.of(finalStateOne), garden);
        inOrder.verifyNoMoreInteractions();
    }
}

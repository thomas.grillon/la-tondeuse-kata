package output;

import mower.MowerState;

import java.util.List;

public interface StateWriter {
    void write(List<MowerState> state);
}

package input;

import mower.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class DefaultGameParserShould {

    @Test
    void parseCorrectlyContent() throws ParsingException {
        //Arrange
        final var actionsListMowerOne = List.of(Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.GO_FORWARD);
        final var actionsListMowerTwo = List.of(Movement.GO_FORWARD, Movement.GO_FORWARD, Movement.TURN_RIGHT, Movement.GO_FORWARD, Movement.GO_FORWARD, Movement.TURN_RIGHT, Movement.GO_FORWARD, Movement.TURN_RIGHT, Movement.TURN_RIGHT, Movement.GO_FORWARD);

        final var parser = new DefaultGameParser();
        final var content = List.of(
                "5 5",
                "1 2 N",
                "GAGAGAGAA",
                "3 3 E",
                "AADAADADDA"
        );
        final var expectedParsedContent = new ParsedContent(
                new Garden(new Dimension(5, 5)),
                List.of(
                        new MowerState(1, new Position(1, 2), Orientation.NORTH, actionsListMowerOne),
                        new MowerState(3, new Position(3, 3), Orientation.EAST, actionsListMowerTwo)
                )
        );

        //Act
        final var actualParsedContent = parser.parse(content);

        //Assertions
        Assertions.assertEquals(expectedParsedContent, actualParsedContent);
    }

    @Test
    void throwParsingExceptionWhenFindingUnknownMovement() {
        //Arrange
        final var parser = new DefaultGameParser();
        final var content = List.of(
                "5 5",
                "1 2 N",
                "GAGAGAGTA",
                "3 3 E",
                "AADAADADDA"
        );

        //Act

        //Assertions
        Assertions.assertThrows(ParsingException.class, () -> parser.parse(content));
    }

    @Test
    void throwParsingExceptionWhenFindingUnknownOrientation() {
        //Arrange
        final var parser = new DefaultGameParser();
        final var content = List.of(
                "5 5",
                "1 2 P",
                "GAGAGAGAA",
                "3 3 E",
                "AADAADADDA"
        );

        //Act

        //Assertions
        Assertions.assertThrows(ParsingException.class, () -> parser.parse(content));
    }
}

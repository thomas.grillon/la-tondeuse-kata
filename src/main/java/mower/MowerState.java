package mower;

import java.util.List;

public record MowerState(int id, Position position, Orientation orientation, List<Movement> actions) {}

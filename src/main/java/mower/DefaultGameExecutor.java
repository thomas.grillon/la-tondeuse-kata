package mower;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultGameExecutor implements GameExecutor {

    private final Garden garden;
    private final List<MowerState> mowers;
    private final MowerExecutor mowerExecutor;

    public DefaultGameExecutor(Garden garden, List<MowerState> initialState, MowerExecutor mowerExecutor) {
        this.garden = garden;
        this.mowers = initialState;
        this.mowerExecutor = mowerExecutor;
    }

    @Override
    public List<MowerState> play() {
        final var states = new LinkedList<List<MowerState>>();
        states.add(this.mowers);

        for (var mower : mowers) {
            final var otherMowerStates = states.getLast()
                    .stream()
                    .filter(mowerState -> (mowerState.id() != mower.id()))
                    .collect(Collectors.toList());

            final var nextState = this.mowerExecutor.moveMower(mower, otherMowerStates, this.garden);
            states.add(nextState);
        }

        return states.getLast();
    }
}

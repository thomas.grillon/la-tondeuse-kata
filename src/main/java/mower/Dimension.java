package mower;

public record Dimension(int x, int y) {}

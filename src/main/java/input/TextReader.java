package input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class TextReader implements Reader {

    private final String filePath;

    public TextReader(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public List<String> read() throws FileNotFoundException {
        try (final var bufferedReader = new BufferedReader(new FileReader(this.filePath))) {
            final var lines = bufferedReader.lines().toList();
            return lines;
        } catch (Exception e) {
            throw new FileNotFoundException();
        }
    }
}

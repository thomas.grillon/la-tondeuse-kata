package input;

public class ParsingException extends Exception {
    public ParsingException(String message) {
        super(message);
    }
}

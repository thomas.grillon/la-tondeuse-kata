package mower;

public interface MowerMover {
    MowerState move(MowerState initialState);
}

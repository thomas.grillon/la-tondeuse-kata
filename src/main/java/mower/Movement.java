package mower;

public enum Movement {
    GO_FORWARD,
    TURN_RIGHT,
    TURN_LEFT,
    MAX_FORWARD
}

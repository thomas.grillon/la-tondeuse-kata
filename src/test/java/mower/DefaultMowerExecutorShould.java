package mower;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultMowerExecutorShould {

    @Mock MowerMover mowerMover;

    @Test
    void executeCorrectlyAllMovements() {
        //Arrange
        final var actionsToPerform = List.of(Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.TURN_LEFT, Movement.GO_FORWARD, Movement.GO_FORWARD);
        final var currentMowerState = new MowerState(1, new Position(1, 2), Orientation.NORTH, actionsToPerform);
        final var otherMowerStates = List.of(new MowerState(2, new Position(3, 3), Orientation.EAST, List.of()));
        final var garden = new Garden(new Dimension(5, 5));

        final var mowerExecutor = new DefaultMowerExecutor(this.mowerMover);
        final var expectedNextState = List.of(
                new MowerState(2, new Position(3, 3), Orientation.EAST, List.of()),
                new MowerState(1, new Position(1, 3), Orientation.EAST, List.of())
        );

        //Stubbing
        when(this.mowerMover.move(any())).thenReturn(new MowerState(1, new Position(1, 3), Orientation.EAST, List.of()));

        //Act
        final var actualNextState = mowerExecutor.moveMower(currentMowerState, otherMowerStates, garden);

        //Assertions
        Assertions.assertEquals(expectedNextState, actualNextState);
    }
}

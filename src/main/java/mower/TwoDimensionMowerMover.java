package mower;

public class TwoDimensionMowerMover implements MowerMover {
    @Override
    public MowerState move(MowerState initialState) {
        final var movement = initialState.actions().get(0);
        final var newActionsList = initialState.actions().subList(1, initialState.actions().size());

        return switch (movement) {
            case GO_FORWARD -> new MowerState(
                    initialState.id(),
                    goForward(initialState.position(), initialState.orientation()),
                    initialState.orientation(),
                    newActionsList
            );
            case TURN_RIGHT -> new MowerState(
                    initialState.id(),
                    initialState.position(),
                    turnRight(initialState.orientation()),
                    newActionsList
            );
            case TURN_LEFT -> new MowerState(
                    initialState.id(),
                    initialState.position(),
                    turnLeft(initialState.orientation()),
                    newActionsList
            );
            case MAX_FORWARD -> new MowerState(
                    initialState.id(),
                    goForward(initialState.position(), initialState.orientation()),
                    initialState.orientation(),
                    initialState.actions()
            );
        };
    }

    private Orientation turnRight(Orientation orientation) {
        return switch (orientation) {
            case NORTH -> Orientation.EAST;
            case SOUTH -> Orientation.WEST;
            case EAST -> Orientation.SOUTH;
            case WEST -> Orientation.NORTH;
        };
    }

    private Orientation turnLeft(Orientation orientation) {
        return switch (orientation) {
            case NORTH -> Orientation.WEST;
            case SOUTH -> Orientation.EAST;
            case EAST -> Orientation.NORTH;
            case WEST -> Orientation.SOUTH;
        };
    }

    private Position goForward(Position position, Orientation orientation) {
        return switch (orientation) {
            case NORTH -> new Position(position.x(), position.y() + 1);
            case SOUTH -> new Position(position.x(), position.y() - 1);
            case EAST -> new Position(position.x() + 1, position.y());
            case WEST -> new Position(position.x() - 1, position.y());
        };
    }
}

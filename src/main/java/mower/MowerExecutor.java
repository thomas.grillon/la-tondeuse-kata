package mower;

import java.util.List;

public interface MowerExecutor {
    List<MowerState> moveMower(MowerState currentMowerState, List<MowerState> otherMowerStates, Garden garden);
}

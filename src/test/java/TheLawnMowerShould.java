import input.DefaultGameParser;
import input.ParsingException;
import mower.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TheLawnMowerShould {

    @Test
    void workCorrectly() throws ParsingException {
        //Arrange
        final var parser = new DefaultGameParser();
        final var mowerMover = new TwoDimensionMowerMover();
        final var mowerExecutor = new DefaultMowerExecutor(mowerMover);
        final var expectedFinalState = List.of(
                new MowerState(1, new Position(1, 3), Orientation.NORTH, List.of()),
                new MowerState(3, new Position(5, 1), Orientation.EAST, List.of())
        );

        //Act
        final var parsedContent = parser.parse(List.of(
                "5 5",
                "1 2 N",
                "GAGAGAGAA",
                "3 3 E",
                "AADAADADDA"
        ));
        final var gameExecutor = new DefaultGameExecutor(parsedContent.garden(), parsedContent.state(), mowerExecutor);
        final var finalState = gameExecutor.play();

        //Assertions
        Assertions.assertEquals(expectedFinalState, finalState);
    }

    @Test
    void handleMaxForwardMovement() throws ParsingException {
        //Arrange
        final var parser = new DefaultGameParser();
        final var mowerMover = new TwoDimensionMowerMover();
        final var mowerExecutor = new DefaultMowerExecutor(mowerMover);
        final var expectedFinalState = List.of(
                new MowerState(1, new Position(1, 2), Orientation.NORTH, List.of()),
                new MowerState(3, new Position(0, 1), Orientation.WEST, List.of()),
                new MowerState(5, new Position(5, 1), Orientation.NORTH, List.of())
        );

        //Act
        final var parsedContent = parser.parse(List.of(
                "5 2",
                "1 2 N",
                "GAGAGAGAA",
                "3 2 E",
                "GGAADAADADAGADDM",
                "4 1 E",
                "AAAAAG"
        ));
        final var gameExecutor = new DefaultGameExecutor(parsedContent.garden(), parsedContent.state(), mowerExecutor);
        final var finalState = gameExecutor.play();

        //Assertions
        Assertions.assertEquals(expectedFinalState, finalState);
    }
}

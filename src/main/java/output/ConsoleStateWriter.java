package output;

import mower.MowerState;
import mower.Orientation;

import java.util.List;

public class ConsoleStateWriter implements StateWriter {
    @Override
    public void write(List<MowerState> state) {
        for (MowerState s : state) {
            System.out.print(String.format("%d %d %s ", s.position().x(), s.position().y(), getOrientation(s.orientation())));
        }
    }

    private String getOrientation(Orientation orientation) {
        return switch (orientation) {
            case NORTH -> "N";
            case SOUTH -> "S";
            case EAST -> "E";
            case WEST -> "W";
        };
    }
}

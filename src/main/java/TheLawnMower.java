import input.DefaultGameParser;
import input.ParsingException;
import input.TextReader;
import mower.DefaultGameExecutor;
import mower.DefaultMowerExecutor;
import mower.TwoDimensionMowerMover;
import output.ConsoleStateWriter;

import java.io.FileNotFoundException;

public class TheLawnMower {
    public static void main(String[] args) throws ParsingException, FileNotFoundException {
        final var mowerMover = new TwoDimensionMowerMover();
        final var mowerExecutor = new DefaultMowerExecutor(mowerMover);

        final var parser = new DefaultGameParser();
        final var reader = new TextReader("src/main/resources/data.txt");
        final var content = reader.read();

        final var parsedContent = parser.parse(content);

        final var gameExecutor = new DefaultGameExecutor(parsedContent.garden(), parsedContent.state(), mowerExecutor);
        final var finalState = gameExecutor.play();

        final var writer = new ConsoleStateWriter();
        writer.write(finalState);
    }
}

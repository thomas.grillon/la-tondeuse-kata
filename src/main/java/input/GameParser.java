package input;

import java.util.List;

public interface GameParser {
    ParsedContent parse(List<String> content) throws ParsingException;
}

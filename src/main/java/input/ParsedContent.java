package input;

import mower.Garden;
import mower.MowerState;

import java.util.List;

public record ParsedContent(Garden garden, List<MowerState> state) {}

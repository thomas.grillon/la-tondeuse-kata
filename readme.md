# Examples
You will find two files in the folder **/resources**

For the first file **data.txt**, the expected result is **1 3 N 5 1 E**

For the second file **data2.txt**, the expected result is **1 2 N 3 1 E 5 1 N** \
The second file is an example with more interaction, more mower and collisions
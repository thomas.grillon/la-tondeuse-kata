package mower;

import java.util.LinkedList;
import java.util.List;

public class DefaultMowerExecutor implements MowerExecutor {

    private final MowerMover mowerMover;

    public DefaultMowerExecutor(MowerMover mowerMover) {
        this.mowerMover = mowerMover;
    }

    @Override
    public List<MowerState> moveMower(MowerState currentMowerState, List<MowerState> otherMowerStates, Garden garden) {
        final var states = new LinkedList<MowerState>();
        states.add(currentMowerState);

        while(!states.getLast().actions().isEmpty()) {
            final var nextState = this.mowerMover.move(states.getLast());
            final var isInGardenBound = garden.isInBound(nextState.position());
            final var isConflictingWithOtherMower = otherMowerStates
                    .stream()
                    .anyMatch(mowerState -> (mowerState.position().equals(nextState.position())));
            final var isNextStateValid = (isInGardenBound && !isConflictingWithOtherMower);

            final var nextStateIfItsNotValid = new MowerState(
                    states.getLast().id(),
                    states.getLast().position(),
                    states.getLast().orientation(),
                    states.getLast().actions().subList(1, states.getLast().actions().size())
            );

            states.add(isNextStateValid ? nextState : nextStateIfItsNotValid);
        }

        final var allNextState = new LinkedList<>(otherMowerStates);
        allNextState.add(states.getLast());

        return allNextState;
    }
}
